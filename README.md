<<<<<<< README.md
# 概要

Docker コンテナにパッケージ管理ツール`poetry`を使って分析環境を構築する。

# 使い方

## ビルドとコンテナ起動

以下のコマンドにより Docker イメージのビルドとコンテナを起動できる。

```
docker-compose up -d
```

_※`Dockerfile`を変更しリビルドしたい場合は`docker-compose up -d --build`を実行する_

## jupyter へのアクセス

コンテナ実行後、以下にアクセスすることで Jupyter lab にアクセスできる。  
<a>http://localhost:8888/lab</a>

## パッケージの追加

以下のコマンドで Python パッケージを追加できる。

```
poetry add <追加したいパッケージ>
```